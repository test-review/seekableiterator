<?php
/**
 * to run from console:
 * 
 *      //default
 *      sudo php test.php "fext.txt" row_number
 * 
 *      //with custom chunk size
 *      sudo php test.php "fext.txt" row_number chunk_size_in_bytes
 * 
 */
class MySeekableIterator implements SeekableIterator {

    private $position;
    private $file;
    private $chunk;


    public function __construct($filename, $chunkSize=32*1024) {
   
        $this->position=0;
        
        $this->chunk=$chunkSize;
        
        $this->file=@fopen($filename, 'r');
        
        if(!$this->file)
        {
            throw new ErrorException("Can not open file ($filename), check exists.\n");
        }
    }
    
    public function __destruct() {
        
        fclose($this->file);
    }

    public function seek($position) 
    {
        if($position <= 0)
        {
            throw new OutOfBoundsException("Invalid seek position ($position)"."\n");
        }

        $this->position = $position;
    }

    public function rewind() {
        rewind($this->file);
        $this->position = 0;
    }

    public function current() {
        
        $offset=0;
        $position=0;
        
        while (!feof($this->file))
        {   
            if(($_str=stream_get_contents($this->file, $this->chunk, $offset))!==false)
            {                
                $new_rows=substr_count($_str, "\n");                
                
                if($position+$new_rows >= $this->position)
                {
                    $_arr=preg_split ("/(\n)/", $_str);                    
                    $_arr_count=count($_arr);
                    
                    for($i=0; $i < $_arr_count; $i++)
                    {
                        if($position+$i+1 == $this->position)
                        {
                            return $_arr[$i];
                        }
                    }                    
                }
                else
                {
                    $position+=$new_rows;
                }
            }
            else
            {
                break;
            }
            
            $offset+=$this->chunk;
        }
        
        $_return ="total positions: {$position};\n";
        $_return.="Invalid seek position ($this->position).";
        
        return $_return;        
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        
        $offset=0;
        $position=0;       
        
        while (!feof($this->file))
        {   
            if(($_str=stream_get_contents($this->file, $this->chunk, $offset))!==false)
            {                
                $new_rows=substr_count($_str, "\n");                
                
                if($position+$new_rows >= $this->position)
                {
                    rewind($this->file);
                    return true;
                }
                else
                {
                    $position+=$new_rows;
                }
            }
            else
            {
                return false;
            }
            
            $offset+=$this->chunk;
        }
        
        rewind($this->file);
        return false;
    }
}

try {

    $start = microtime(true);
    
    $it = new MySeekableIterator($argv[1], isset($argv[3])?$argv[3]:32*1024);
    
    $it->seek($argv[2]);
    
    echo "\n".$it->current()."\n\n";
    
    echo "time: ".round(microtime(true) - $start, 4)." sec.\n\n";
    
} catch (ErrorException $e) {
    echo $e->getMessage();
}
catch (OutOfBoundsException $e) {
    echo $e->getMessage();
}